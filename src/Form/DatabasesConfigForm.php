<?php

namespace Drupal\multiple_databases\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

include_once DRUPAL_ROOT . '/core/includes/install.inc';

/**
 * Class DatabasesConfigForm.
 */
class DatabasesConfigForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'multiple_databases.databasesconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'databases_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entityType = \Drupal::entityTypeManager()->getStorage('database_infos');
    $request = \Drupal::request();
    $drivers = drupal_get_database_types();
    $database_types = [];
    foreach($drivers as $key => $driver) {
      $database_types[$key] = $driver->name();
    }

    $defaults = [
      'id' => '',
      'database_id' => '',
      'database_type' => '',
      'database_name' => '',
      'password' => '',
      'host' => '',
      'username' => '',
      'port' => '',
//      'namespace' => '',
      'prefix' => ''
    ];
    if ($id = $request->query->get('id')) {
      $entity = $entityType->load($id);
      foreach($defaults as $field => $value) {
        $defaults[$field] = $entity->get($field)->value;
      }

      $form['entity_id'] = [
        '#type' => 'hidden',
        '#value' => $id,
      ];
    }

    $form['database_type'] = [
      '#type' => 'select',
      '#options' => $database_types,
      '#default_value' => $defaults['database_type'],
      '#title' => $this->t('Database Type'),
    ];

    $form['database_id'] = [
      '#type' => 'machine_name',
      '#required' => true,
      '#default_value' => $defaults['database_id'],
      '#title' => $this->t('Database connect id'),
    ];

    $form['database_name'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#default_value' => $defaults['database_name'],
      '#title' => $this->t('Database name'),
    ];

    $form['host'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#default_value' => $defaults['host'],
      '#title' => $this->t('Host'),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#default_value' => $defaults['username'],
      '#title' => $this->t('Username'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#default_value' => $defaults['password'],
      '#title' => $this->t('Password'),
    ];

    $form['port'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#default_value' => $defaults['port'],
      '#title' => $this->t('Port'),
      '#description' => 'Mysql default 3306, Oracle default is 1521'
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#default_value' => $defaults['prefix'],
      '#title' => $this->t('Prefix'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#default_value' => $defaults['database_name'],
      '#value' => $this->t('Submit')
    ];

    $form['table'] = array(
      '#type' => 'table',
      '#header' => array(
        'id' => $this->t('Database id'),
        'database_type' => $this->t('Database Type'),
        'database_name' => $this->t('Database Name'),
        'host' => $this->t('Host'),
        'username' => $this->t('username'),
        'port' => $this->t('Database port'),
        'namespace' => $this->t('Namespace'),
        'operation' => $this->t('Operation')
      ),
    );

    foreach($entityType->loadMultiple() as $row => $entity) {
      $row_data['id'] = $entity->get('database_id')->value;
      $row_data['database_type'] = $entity->get('database_type')->value;
      $row_data['database_name'] = $entity->get('database_name')->value;
      $row_data['username'] = $entity->get('username')->value;
      $row_data['host'] = $entity->get('host')->value;
      $row_data['port'] = $entity->get('port')->value;
      $row_data['namespace'] = $entity->get('namespace')->value;

      $edit_url = Url::fromRoute('multiple_databases.databases_config_form');
      $edit_url->setOption('query', [
        'id' => $entity->id(),
      ]);

      $del_url = Url::fromRoute('entity.database_infos.delete_form', ['database_infos' => $entity->id()]);
      $del_url->setOption('query', \Drupal::destination()->getAsArray());

      $test_url = Url::fromRoute('multiple_databases.database_test_connect_controller_index');
      $test_url->setOption('query', [
        'id' => $entity->get('database_id')->value,
      ]);

      $row_data['operation'] = [
        'data' => new FormattableMarkup('<a href=":edit_link">@edit_name</a> | <a href=":del_link">@del_name</a> | <a href=":test_link" target="_blank">@test_name</a>',
          [
            ':edit_link' => $edit_url->toString(),
            '@edit_name' => $this->t('Edit'),
            ':del_link' => $del_url->toString(),
            '@del_name' => $this->t('Delete'),
            ':test_link' => $test_url->toString(),
            '@test_name' => $this->t('Test Connect'),
          ]
        )];
      $form['table']['#rows'][] = $row_data;
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entityType = \Drupal::entityTypeManager()->getStorage('database_infos');
    $exists = $entityType->getQuery();
    $exists->condition('database_id', $form_state->getValue('database_id'));
    if ($id = $form_state->getValue('entity_id')) {
      $exists->condition('id', $id, '<>');
    }

    if ($exists->execute()) {
      $form_state->setError($form['database_id'], $this->t('the database id exists'));
    }

  }

  /**
   * {@inheritdoc}
   * @throws \ReflectionException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entityType = \Drupal::entityTypeManager()->getStorage('database_infos');
    $drivers = drupal_get_database_types();
    $database_type = $form_state->getValue('database_type');
    $reflection = new \ReflectionClass($drivers[$database_type]);
    $install_namespace = $reflection->getNamespaceName();
    $namespace = substr($install_namespace, 0, strrpos($install_namespace, '\\'));

    $data = [
      'database_id' => $form_state->getValue('database_id'),
      'database_type' => $database_type,
      'database_name' => $form_state->getValue('database_name'),
      'prefix' => $form_state->getValue('prefix'),
      'host' => $form_state->getValue('host'),
      'username' => $form_state->getValue('username'),
      'port' => $form_state->getValue('port'),
      'namespace' => $namespace,
    ];

    if ($pass = $form_state->getValue('password')) {
      $data['password'] = $pass;
    }

    if ($id = $form_state->getValue('entity_id')) {
      $entity = $entityType->load($id);
      foreach($data as $key => $value) {
        $entity->set($key, $value);
      }
      $entity->save();
    } else {
      $entityType->create($data)->save();
    }
    $form_state->setRedirect('multiple_databases.databases_config_form');
    \Drupal::messenger()->addMessage($this->t('Success'));
  }

}
