<?php

namespace Drupal\multiple_databases\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

/**
 * Class DatabaseTestConnectController.
 */
class DatabaseTestConnectController extends ControllerBase {

  /**
   * Index.
   *
   * @return string
   *   Return Hello string.
   */
  public function index() {
    $request = \Drupal::request();
    $messager = \Drupal::messenger();
    $id = $request->query->get('id');
    if (!$id) {
      $messager->addError($this->t('The ID parameter does not exist'));
    }

    try {
      try {
        get_database_connection($id);
        $messager->addMessage($this->t('Database connection successful'));
      } catch (\Exception $e) {
        $messager->addError($e->getMessage());
      }
    } catch (\Throwable $e) {
      $messager->addError($e->getMessage());
    }

    $entity = \Drupal::entityTypeManager()->getStorage('database_infos')->loadByProperties([
      'database_id' => $id
    ]);

    $entity = reset($entity);
    $database =  [
      'database' => $entity->get('database_name')->value,
      'username' => $entity->get('username')->value,
      'password' => $entity->get('password')->value,
      'prefix' => $entity->get('prefix')->value,
      'host' => $entity->get('host')->value,
      'port' => $entity->get('port')->value,
      'namespace' => $entity->get('namespace')->value,
      'driver' => $entity->get('database_type')->value,
    ];

    \Drupal::messenger()->addStatus(Markup::create("<pre>" . print_r($database, 1)));

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Database test ' . $id)
    ];
  }

}
