<?php

namespace Drupal\multiple_databases\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Database infos entity.
 *
 * @ingroup multiple_databases
 *
 * @ContentEntityType(
 *   id = "database_infos",
 *   label = @Translation("Database infos"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "database_infos",
 *   translatable = FALSE,
 *   admin_permission = "administer database infos entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "delete-form" = "/admin/multiple_databases/{database_infos}/delete",
 *   },
 * )
 */
class DatabaseInfos extends ContentEntityBase{

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('database_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($id) {
    $this->set('database_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['database_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database connection id'))
      ->setDescription(t('the database id.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['database_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database Type'))
      ->setDescription(t('database Type.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['database_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database name'))
      ->setDescription(t('database name.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['host'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database host'))
      ->setDescription(t('database host.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['username'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database username'))
      ->setDescription(t('database username.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['password'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database password'))
      ->setDescription(t('database password.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['port'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database port'))
      ->setDescription(t('database server port.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['namespace'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database namespace'))
      ->setDescription(t('database namespace. Drupal\Driver\Database\oracle'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['prefix'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Database prefix'))
      ->setDescription(t('database prefix.'))
      ->setSettings([
        'max_length' => 50
      ])
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
